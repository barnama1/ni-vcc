**1. Nasazený OpenStack, kterému něco chybí, např. nefunguje síť nebo nespouští VM, nebo je jen na 1 uzlu.**

**2. Nasazený OpenStack na 2 uzlech (řídící a výpočetní), jak bylo předvedeno na cvičení. Musí spustit virtuální stroj, který dostane IP adresu z vestavěného DHCP na řídícím uzlu.**

**3. OpenStack, který dokáže spouštět VM na dvou uzlech (buď druhý výpočetní nebo služba compute na řídícím uzlu). VM musí na sebe dokázat pingovat.**
  - viz screenshoty
  - případně by mělo být dostupné přes webové rozhraní na 10.38.6.178 (user:admin password:jBcJXMkLkhDDEzeVcVxeJyOiymLTglucteAKazcD)