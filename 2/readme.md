**1. Nalezení xml deskriptoru virtuálního stroje a v něm jména rozhraní. Trasování provozu mezi 2 VM nebo mezi VM a routerem nebo DHCP serverem na rozhraní příslušející na hypervizoru virtuálnímu stroji.**
  - xml deskriptor instance je přístupný z containeru "nova-libvirt" v /etc/libvirt/qemu/
  - v deskriptoru najdu jméno rozhraní (tap)
  - tcpdump viz screenshot 1_tcpdump.png

**2. Trasování provozu tunelovaného pomocí VXLAN mezi dvěma uzly, a to v zobrazené v grafickém nástroji Wireshark.**
  - viz screenshoty 2_wireshark.png, 2_tcpdump.png, 2_os.png

**3. Nakreslit diagram síťových prvků, přes které prochází east-west provoz (mezi dvěma VM na různých hypervizorech), a to s konkrétními jmény rozhraní ve Vaši instalaci OpenStacku (doloženo namátkově screenshoty).**
  - viz 3_schema.pdf a screenshoty 3_bridge.png, 3_openvswitch.png, 3_ports.png
  - provoz jde z VNIC instance 1 (tap), přes firewall (linux bridge qbr) do openvswitche (skrz virtual ethernet qvb-qvo); V rámci openvswitche jde přes integration bridge do tunnel bridge, na kterém má VTEP pro každý node