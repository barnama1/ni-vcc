**1. Sestavení obrazu s nějakou webovou aplikací metodou Dockerfile. Pro inspiraci použít návod na Wordpress ze cvičení.**
  - zvolil jsem primitivní webovou aplikaci vytvořenou pomocí generátoru Express (viz test-app)
  - na obrázku 1_build.png je vidět sestavování a spouštění kontejneru, na obrázcích 1_gui.png a 1_terminal.png je potom vidět výsledek, tedy běžící webová aplikace
  - dockerfile lze najít v test-app/Dockerfile

**2. Analýza obrazu ve formátu OCI získaného z předchozího kroku pomocí "docker save". Které vrstvy odpovídají podkladovému obrazu a které příkazům z Dockerfile?**
  - pomocí příkazu "docker history" lze sledovat, jak jednotlivé příkazy z Dockerfile vytvářejí různé vrstvy (viz 2_history.png).
  - Vrstva "aa67ba258e18" odpovídá podkladovému obrazu (příkaz FROM node:10-alpine), vrstvy nad ní potom odpovídají ostatním příkazům z Dockerfile.
  - na obrázku 2_dive.png je vidět výstup utility Dive, která umožňuje interaktivní prohledávání obsahu jednotlivých vrstev. Můžeme tak mimo jiné zjistit jména souborů, ve kterých jsou dané vrstvy uložené.
  - na obrázku 2_fs.png je vidět obsah vrstvy odpovídající přikazu "WORKDIR /usr/src/test-app", je vidět že vrstva obsahuje pouze danou složku a nic jiného, což opdovídá jejímu vytvoření

**3. Vytvoření systémového kontejneru s jinou distribucí než Debian a Ubuntu. Pro inspiraci lze použít systemd-nspawn předvedený na cvičení.**
  - vytvořil jsem kontejner s ArchLinux pomocí systemd-nspawn
  - na obrázku 3_boot.png je vidět proces bootování na uzlu s Ubuntu